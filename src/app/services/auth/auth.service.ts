import { Injectable } from '@angular/core';
import { MainService } from '../main/main.service';
import { CanActivate } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class AuthService implements CanActivate{

  constructor(public mainService: MainService) {}
 
  canActivate(): boolean {
    return this.mainService.isAuthenticated();
  }
}
