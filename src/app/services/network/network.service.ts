import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class NetworkService {

  isConnected: boolean = false;
  constructor(
    private http: HttpClient
  ) { 
    this.initNetworkEvent();
    window.addEventListener('online', this.initNetworkEvent);
    window.addEventListener('offline', this.initNetworkEvent);
  }

  initNetworkEvent() {
    if (navigator.onLine) {
      this.isConnected = true;
      // alert('isconnected ==> ' + this.isConnected);
    }
    else {
      this.isConnected = false;
      // alert('isconnected ==>' + this.isConnected);
    }
  }

  isOnline() {
    return this.isConnected;
  }
}
