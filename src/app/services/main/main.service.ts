import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { LocalStorageService } from '../localStorage/local-storage.service';

@Injectable({
  providedIn: 'root'
})
export class MainService {

  token;
  username;
  headUrl = "http://192.168.111.190:5555/";

  constructor(
    public http: HttpClient,
    private lsService: LocalStorageService
    ) { }

    //parameter-r orj irsen path-iin daguu file unshina.
  readfile(path) {
    let promise = new Promise((resolve, reject) => {
      this.http.get(path)
        .toPromise()
        .then(res => {
          // console.log("--<>--MAIN PROVIDER--<>--", res);
          resolve(res);
        },
          err => {
            console.log(err);
            reject(err);
          });
    });
    return promise;
  }

  //ehnii parameter-r orj irsen path-iin daguu server luu get huselt tavih ba
  //2 dah parameter-r orj irsen options-g huselted oruulna.
  get(path, options) {
    // console.log(JSON.stringify(new Date()));
    let promise = new Promise((resolve, reject) => {
      this.http.get(this.headUrl + path, options)
        .toPromise()
        .then(res => {
          // console.log(res);
          resolve(res);
        },
          err => {
            console.log(err);
            reject(err);
          });
    });
    return promise;
  }

  //ehnii parameter-r orj irsen data-g
  //2 dah parameter-r orj irsen path-iin daguu server luu post huselt ilgeeh ba
  //3 dah parameter-r orj irsen options-g huselted oruulan.
  post(data, path, options) {
    // options.headers = new HttpHeaders().append('Authorization', this.token);
    // options.params = new HttpParams().set("username", this.username);
    // console.log('\n',data);
    // console.log('\n',this.headUrl + path);
    let promise = new Promise((resolve, reject) => {
      this.http.post(this.headUrl + path, data, options)
        .toPromise()
        .then(res => {
          // console.log(res);
          resolve(res);
        },
          err => {
            console.log(err);
            reject(err);
          });
    });
    return promise;
  }

  login(data) {
    this.username = data.username;
    let promise = new Promise((resolve, reject) => {
      this.http.post(this.headUrl + "pwd", data, {})
        .toPromise()
        .then((res: any) => {
          this.token = res.token;

          // this.lsService.set("username", data.username);
          // this.lsService.set("token", res.token);
          this.lsService.set("username", "admin");
          this.lsService.set("authenticated", true);

          // this.socketProvider.sendUserNameToService(this.username);

          resolve(res);
        },
          err => {
            // console.log('Error! ---> ',err);
            reject(err);
          });
    });
    return promise;
  }

  logout() {
    this.lsService.delete("token");
    this.lsService.delete("username");
    this.lsService.delete("authenticated");
  }

  forceLogOut() {
    this.logout();
    window.location.reload();
  }

  getToken() {
    return this.token;
  }
  getUsername() {
    if(this.username != undefined)
      return this.username;
    else
      return this.lsService.get("username");
  }

  isAuthenticated(){
    // if(this.lsService.get("token") != null)
    //   return true;
    // else
    //   return false;
    return true;
  }
}
