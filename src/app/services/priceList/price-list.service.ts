import { Injectable } from '@angular/core';
import * as _ from 'lodash';
import { MainService } from '../main/main.service';
import { NetworkService } from '../network/network.service';
import { LocalStorageService } from '../localStorage/local-storage.service';

@Injectable({
  providedIn: 'root'
})
export class PriceListService {

  private priceList : Array<any>;
  private ls_key_name: "price_list";
  constructor(
    private mainService: MainService,
    private network: NetworkService,
    private lsService: LocalStorageService
  ) { }

  //File unshih
  //
  private fetchDataFromFile() {
    let promise = new Promise((resolve, reject) => {
      this.mainService.readfile('../assets/data/price_list.json')
        .then(res => {
          this.priceList = (res as any).result;
          resolve(res);
        },
          err => {
            console.log(err);
            reject(err);
          });
    });
    return promise;
  }
  //Server-s data duudah
  private fetchDataFromServer() {
    let username = this.mainService.getUsername();
    let promise = new Promise((resolve, reject) => {
      this.mainService.get("products", {})
        .then(res => {
          this.priceList = (res as any).result;
          // console.log(res);
          resolve(res);
        },
          err => {
            console.log(err);
            reject(err);
          });
    });
    return promise;
  }

  getPriceList(){
    let promise = new Promise((resolve, reject) => {
      if(!_.isUndefined(this.priceList)){
        resolve(this.priceList);
      }
      else {
        if (!this.network.isOnline) {
          // console.log('getPartners not online! call here data from local storage');
          let data = this.lsService.get(this.ls_key_name);
          resolve(data);
        }
        else {
          // this.fetchProductDataFromServer().then(data => {
          this.fetchDataFromFile().then(data => {
            this.lsService.set(this.ls_key_name, data);
            resolve(data);
          });
        }
      }
    });
    return promise;
    
  }
}
