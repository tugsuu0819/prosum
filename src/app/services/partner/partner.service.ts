import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import * as _ from 'lodash';

import { MainService } from '../main/main.service';
import { NetworkService } from '../network/network.service';
import { LocalStorageService } from '../localStorage/local-storage.service';

import { PARTNERS } from '../../data/partners';
import { Partner } from '../../models/partner';


@Injectable({
  providedIn: 'root'
})
export class PartnerService {

  private partners: Partner[];
  private ls_key_name: "partners";
  constructor(
    private mainService: MainService,
    private network: NetworkService,
    private lsService: LocalStorageService
  ) {
    // console.log('PartnerService');
    this.fetchPartnerDataFromFile();
  }

  //File unshih
  //
  private fetchPartnerDataFromFile() {
    let promise = new Promise((resolve, reject) => {
      this.mainService.readfile('../assets/data/partners.json')
        .then(res => {
          this.partners = (res as any).result;
          // console.log(res, this.partners);
          resolve(this.partners);
        },
          err => {
            console.log(err);
            reject(err);
          });
    });
    return promise;
  }
  //Server-s data duudah
  private fetchPartnerDataFromServer() {
    let username = this.mainService.getUsername();
    let promise = new Promise((resolve, reject) => {
      this.mainService.get("partners", {})
        .then(res => {
          this.partners = (res as any).result;
          // console.log(res);
          resolve(this.partners);
        },
          err => {
            console.log(err);
            reject(err);
          });
    });
    return promise;
  }
  getPartnersFromService() : Observable<Partner[]> {
    this.partners = PARTNERS;
    return of(this.partners);
  }
  // getPartners(){
  //   let promise = new Promise((resolve, reject) => {
  //     if(!_.isUndefined(this.partners)){
  //       resolve(this.partners);
  //     }
  //     else {
  //       if (!this.network.isOnline) {
  //         // console.log('getPartners not online! call here data from local storage');
  //         let data = this.lsService.get(this.ls_key_name);
  //         resolve(data);
  //       }
  //       else {
  //         // this.fetchPartnerDataFromServer().then(data => {
  //         this.fetchPartnerDataFromFile().then(data => {
  //           this.lsService.set(this.ls_key_name, data);
  //           resolve(data);
  //         });
  //       }
  //     }
  //   });
  //   return promise;
  // }
  getPartners(){
    return this.partners;
  }

  //parameter-eer orj irsen id-d hargalzah partner-iin medeelliig butsaana.
  getPartner(id){
    // console.log(id, this.partners);
    return _.find(this.partners, function(o){ return o.id == id; });
    // if(_.isUndefined(partner))
    //   return "";
    // return partner.name;
  }
}
