import { TestBed } from '@angular/core/testing';

import { PartnerResolverService } from './partner-resolver.service';

describe('PartnerResolverService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PartnerResolverService = TestBed.get(PartnerResolverService);
    expect(service).toBeTruthy();
  });
});
