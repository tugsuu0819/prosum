import { Injectable } from '@angular/core';
import { Router, Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { Observable }  from 'rxjs';

import { Partner } from '../../models/partner';
import { PartnerService } from './partner.service';

@Injectable({
  providedIn: 'root'
})
export class PartnerResolverService implements Resolve<Partner[]> {
  constructor(
    private partnerService : PartnerService
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Partner[]> {
    return this.partnerService.getPartnersFromService();
  }
}
