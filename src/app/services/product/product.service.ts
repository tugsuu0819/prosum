import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import { MainService } from '../main/main.service';
import { NetworkService } from '../network/network.service';
import { LocalStorageService } from '../localStorage/local-storage.service';
import * as _ from 'lodash';

import { Product } from '../../models/product';
import { PRODUCTS } from '../../data/product';

import { PriceList, PartnerPriceList } from '../../models/price-list';
import { PRICELIST } from '../../data/price-list';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private products: Array<any>;
  private pricelist: PriceList[];
  private ls_key_name: string = "products";
  constructor(
    private mainService: MainService,
    private network: NetworkService,
    private lsService: LocalStorageService
  ) {
  }

  //File unshih
  //
  private fetchProductDataFromFile() {
    let promise = new Promise((resolve, reject) => {
      this.mainService.readfile('../assets/data/products.json')
        .then(res => {
          this.products = (res as any).result;
          resolve(this.products);
        },
          err => {
            console.log(err);
            reject(err);
          });
    });
    return promise;
  }
  //Server-s data duudah
  private fetchProductDataFromServer() {
    let username = this.mainService.getUsername();
    let promise = new Promise((resolve, reject) => {
      this.mainService.get("products", {})
        .then(res => {
          this.products = (res as any).result;
          // console.log(res);
          resolve(this.products);
        },
          err => {
            console.log(err);
            reject(err);
          });
    });
    return promise;
  }

  // getProducts() : Promise<any> {
  //   let promise = new Promise((resolve, reject) => {
  //     if(!_.isUndefined(this.products)){
  //       resolve(this.products);
  //     }
  //     else {
  //       if (!this.network.isOnline) {
  //         // console.log('getPartners not online! call here data from local storage');
  //         let data = this.lsService.get(this.ls_key_name);
  //         resolve(data);
  //       }
  //       else {
  //         // this.fetchProductDataFromServer().then(data => {
  //         this.fetchProductDataFromFile().then(data => {
  //           this.lsService.set(this.ls_key_name, data);
  //           resolve(data);
  //         });
  //       }
  //     }
  //   });
  //   return promise;
  // }
  getProductsFromServer() : Observable<Product[]> {
    this.products = PRODUCTS;
    this.pricelist = PRICELIST;
    return of(this.products);
  }
  getProducts() : Product[] {
    return this.products;
  }
  getProduct(id: number) : Product {
    return this.products.find(product => product.id === id);
  }
  getPrice(partnerId: number, productId: number) : number {
    let partner_pr =  _.find(this.pricelist, {partnerId: partnerId});
    if(!partner_pr) return 0;
    let productPrice = _.find(partner_pr.priceList, {id: productId});
    if(!productPrice) return 0;
    return productPrice.fixed_price;
  }
}
