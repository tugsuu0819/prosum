import { Injectable } from '@angular/core';
import { Router, Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { Observable, of, EMPTY }  from 'rxjs';

import { Product } from '../../models/product';
import { ProductService } from './product.service';

@Injectable({
  providedIn: 'root'
})
export class ProductResolverService implements Resolve<Product[]> {

  constructor(
    private productService : ProductService
  ) { }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) : Observable<Product[]> {
    return this.productService.getProductsFromServer();
  }
}
