import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  constructor() { }

  /* //ehnii parameter-r orj irsen key-r local storage dotroos data-g olood
  //2 dah parameter-r orj irsen data-tai haritsuulan zuwiig ni olood replace hiigeed
  //butsaagaad hadgalna
  replace_purchase_item(key, data) {
    let ls_data = this.get(key);
    if (ls_data) { this.delete(key); };
    let index = _.findIndex(ls_data, function (o) { return o.getpurchaserequisition.id == data.id; });
    if (index != -1) {
      ls_data.getpurchaserequisition = data;
    }
    this.set(key, ls_data);
  } */

  set(key, value): void {
    localStorage.setItem(key, JSON.stringify(value));
  }

  get(key): any {
    return JSON.parse(localStorage.getItem(key));
  }

  delete(key): void {
    localStorage.removeItem(key);
  }

  deleteAll(): void {
    localStorage.clear();
  }
}
