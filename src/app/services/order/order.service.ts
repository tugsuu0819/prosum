import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import * as _ from 'lodash';
import * as moment from 'moment';

import { NetworkService } from '../network/network.service';
import { LocalStorageService } from '../localStorage/local-storage.service';

import { MainService } from '../main/main.service';
import { PartnerService } from '../partner/partner.service';
import { Order } from '../../models/order';
import { ORDERS } from '../../data/order';
import { OrderCreate, OrderCreateLine } from '../../models/order-create';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  private orders: Array<Order>;
  private ls_key_name: string = "orders";
  private createobj : OrderCreate;
  constructor(
    private mainService: MainService,
    private network: NetworkService,
    private lsService: LocalStorageService,
    private partnerService: PartnerService
    ) {}

  //File unshih
  //
  private fetchOrderDataFromFile() {
    let promise = new Promise((resolve, reject) => {
      this.mainService.readfile('../assets/data/order.json')
        .then(res => {
          // this.orders = (res as any).result;
          // // console.log(res);
          // resolve(this.orders);
          resolve((res as any).result);
        },
          err => {
            console.log(err);
            reject(err);
          });
    });
    return promise;
  }
  //Server-s data duudah
  private fetchOrderDataFromServer() {
    let username = this.mainService.getUsername();
    let promise = new Promise((resolve, reject) => {
      this.mainService.post({ user_name: username }, "orders", {})
        .then(res => {
          // this.orders = (res as any).result;
          // // console.log(res);
          // resolve(this.orders);
          resolve((res as any).result);
        },
          err => {
            console.log(err);
            reject(err);
          });
    });
    return promise;
  }
  private prepOrders(resp){
    let ret = [];
    _.forEach(resp, function(order){
      order.itemCount = _.size(order.ln);
      order.date = moment(order.dt_ord).format("YYYY.MM.DD HH:mm");
      ret.push(order);
    });
    return ret;
  }
  public getOrdersFromServer() : Observable<Order[]>{
    this.orders = this.prepOrders(ORDERS);
    // console.log(this.orders);
    return of(this.orders);
  }
  public getOrders() : Order[] {
    // let promise = new Promise((resolve, reject) => {
    //   if (!_.isUndefined(this.orders)) {
    //     resolve(this.orders);
    //   }
    //   else {
    //     if (!this.network.isOnline) {
    //       console.log('getORders not online!');
    //       this.orders = this.prepOrders(this.lsService.get(this.ls_key_name));
    //       resolve(this.orders);
    //     }
    //     else {
    //       // // this.fetchOrderDataFromServer().then(data => {
    //       // this.fetchOrderDataFromFile().then(data => {
    //       //   this.lsService.set(this.ls_key_name, data)
    //       //   this.orders = this.prepOrders(data);
    //       //   // this.lsService.set(this.ls_key_name, data);
    //       //   resolve(this.orders);
    //       // });
    //       // this.fetchOrderDataFromServer().then(data => {
    //       this.fetchOrderDataFromFile().then(data => {
    //         this.lsService.set(this.ls_key_name, data)
    //         this.orders = this.prepOrders(data);
    //         // this.lsService.set(this.ls_key_name, data);
    //         resolve(this.orders);
    //       });
    //     }
    //   }
    // });
    // return promise;

    // this.orders = ORDERS;
    // _.forEach(temp, (o) => {
    //   let item: any = {
    //     date: moment(o.dt).format("YYYY-MM-DD"),
    //     time: moment(o.dt).format("HH:mm"),
    //     no_pr: _.size(o.ln)
    //   };
    //   if (o.st == "draft")
    //     item.st = "Хүлээгдэж байгаа";
    //   else if (o.st == "sale")
    //     item.st = "Зарагдсан";
    //   else if (o.st == "done")
    //     item.st = "Хүргэгдсэн";
    //   else { }
    //   item.pn = this.partnerService.getPartner(o.pid);
    //   // console.log(item);
    //   // temp.push(item);
    //   // console.log(moment(o.dt).format("YYYY-MM-DD"), moment(o.dt).format("HH:mm"));
    //
    //   // this.items.push(item);
    // });
    return this.orders;
  }
  getOrder(id: number): Order {
    return _.find(this.orders, {'id': id});
  }
  create_o_set_partner(partnerId : number){
    this.createobj = new OrderCreate();
    this.createobj.partner_id = partnerId;
  }
  get_create_o_partner(){
    return this.createobj.partner_id;
  }
  create_o_set_orderline(order_lines : OrderCreateLine[]){
    if(!_.isArray(this.createobj.order_line))
      this.createobj.order_line = [];
    this.createobj.order_line = order_lines;
    // if(_.isObject(product))
    //   this.createobj.order_line.push(product);
  }
  get_create_o_data(){
    return this.createobj;
  }
}
