import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NavService {
  private param: any;
  constructor() { }

  set(param){
    this.param = param;
  }

  get(){
    return this.param;
  }
}
