import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { MainService } from './services/main/main.service';
import { MenuController } from '@ionic/angular';

// import { Platform } from '@ionic/angular';
// import { SplashScreen } from '@ionic-native/splash-screen/ngx';
// import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  public appPages = [
    // {
    //   title: 'Home',
    //   url: '/home',
    //   icon: 'home'
    // },
    // {
    //   title: 'List',
    //   url: '/list',
    //   icon: 'list'
    // },
    {
      title: 'Order',
      url: '/members/order',
      icon: 'list'
    }
  ];

  constructor(
    // private platform: Platform,
    // private splashScreen: SplashScreen,
    // private statusBar: StatusBar
    private mainService: MainService,
    private router: Router,
    private menuCtrl: MenuController
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.menuCtrl.enable(true);
    // if(this.mainService.isAuthenticated())
    //   this.router.navigate(['members', 'order']);
    // else
    //   this.router.navigate(['login']);
    // this.platform.ready().then(() => {
    //   this.statusBar.styleDefault();
    //   this.splashScreen.hide();
    // });
  }
}
