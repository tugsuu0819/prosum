import { OrderLine } from './order-line';

export class Order {
  id: number;
  comp_name: string;
  user_id: number;
  p_id: number;
  dt_ord: string;
  so_name: string;
  pl_id: number;
  am_untax: number;
  am_total: number;
  am_tax: number;
  st: string;
  ln: Array<OrderLine>;
}
