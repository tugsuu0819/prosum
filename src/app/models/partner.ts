export class Partner {
  id: number;
  name: string;
  phone: string;
  vat: string;
  ref: string;
  address: string;
}
