export class Product{
  id: number;
  barcode: string;
  name: string;
  list_price: number;
  qty_available: number;
  qty_v: number;
  product_qty?: number;
  product_price?: number;
  product_tPrice? : number;
}
