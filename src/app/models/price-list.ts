export class PriceList {
  partnerId: number;
  priceList: PartnerPriceList[];
}
export class PartnerPriceList {
  id: number;
  name: string;
  barcode: string;
  fixed_price: number;
  min_quantity: number;
  date_start: string;
  date_end: string;
}
