export class OrderLine {
  id: number;
  prod_id: number;
  qty: number;
  ord_id: number;
  pu: number;
  pn: string;
}
