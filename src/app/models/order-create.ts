export class OrderCreate {
  partner_id: number;
  order_line: OrderCreateLine[];
}

export class OrderCreateLine {
  product_id: number;
  product_qty: number;
  price?: number;
  
}
