import { Product } from '../models/product';

export const PRODUCTS : Product[] =
[
  {
      "id": 1,
      "barcode": "4003240471001",
      "name": "Albi алим 200мл",
      "list_price": 2500,
      "qty_available": 1401,
      "qty_v": 110
  },
  {
      "id": 2,
      "barcode": "4003240470004",
      "name": "Albi жүрж 200мл",
      "list_price": 2500,
      "qty_available": 1489,
      "qty_v": 22
  },
  {
      "id": 3,
      "barcode": "4001757016135",
      "name": "Weinrich's 1895 70% какао",
      "list_price": 5500,
      "qty_available": 482,
      "qty_v": 33
  }
]
