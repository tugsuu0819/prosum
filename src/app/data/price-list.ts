import { PriceList } from '../models/price-list';

export const PRICELIST : PriceList[] =
[
  {
    "partnerId": 7,
    "priceList": [
        {
            "id": 1,
            "name": "Albi алим 200мл",
            "barcode": "4003240471001",
            "fixed_price": 2125,
            "min_quantity": 0,
            "date_start": null,
            "date_end": null
        },
        {
            "id": 2,
            "name": "Albi жүрж 200мл",
            "barcode": "4003240470004",
            "fixed_price": 2125,
            "min_quantity": 0,
            "date_start": null,
            "date_end": null
        }
    ]
  },
  {
    "partnerId": 8,
    "priceList": [
        {
            "id": 1,
            "name": "Albi алим 200мл",
            "barcode": "4003240471001",
            "fixed_price": 2250,
            "min_quantity": 0,
            "date_start": null,
            "date_end": null
        },
        {
            "id": 2,
            "name": "Albi жүрж 200мл",
            "barcode": "4003240470004",
            "fixed_price": 2250,
            "min_quantity": 0,
            "date_start": null,
            "date_end": null
        },
        {
            "id": 3,
            "name": "Weinrich's 1895 70% какао",
            "barcode": "4001757016135",
            "fixed_price": 4950,
            "min_quantity": 0,
            "date_start": null,
            "date_end": null
        }
    ]

  },
  {
    "partnerId": 9,
    "priceList": [
      {
          "id": 1,
          "name": "Albi алим 200мл",
          "barcode": "4003240471001",
          "fixed_price": 2000,
          "min_quantity": 0,
          "date_start": null,
          "date_end": null
      },
      {
          "id": 2,
          "name": "Albi жүрж 200мл",
          "barcode": "4003240470004",
          "fixed_price": 2000,
          "min_quantity": 0,
          "date_start": null,
          "date_end": null
      },
      {
          "id": 3,
          "name": "Weinrich's 1895 70% какао",
          "barcode": "4001757016135",
          "fixed_price": 4400,
          "min_quantity": 0,
          "date_start": null,
          "date_end": null
      }
    ]
  }
];
