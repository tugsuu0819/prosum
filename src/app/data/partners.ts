import { Partner } from '../models/partner';

export const PARTNERS : Partner[] =
[
  {
    "id": 9,
    "name": "Номин Тав Трейд ХХК",
    "phone": null,
    "vat": null,
    "ref": null,
    "address": " "
  },
  {
    "id": 8,
    "name": "Найра Кафе",
    "phone": null,
    "vat": null,
    "ref": null,
    "address": " "
  },
  {
    "id": 7,
    "name": "Алтан жолоо ХХК",
    "phone": "+976 7705 5959",
    "vat": null,
    "ref": null,
    "address": "Ulaanbaatar 3-р хороо, Баянзүрх дүүрэг, Сансар үйлчилгээний төв, 12-р хороолол,"
  }
]
