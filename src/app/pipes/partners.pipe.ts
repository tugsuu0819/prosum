import { Pipe, PipeTransform } from '@angular/core';

import { Partner } from '../models/partner';

@Pipe({
  name: 'partnersPipe'
})
export class PartnersPipe implements PipeTransform {

  transform(partners: Partner[], value: string): Partner[] {
    if(!partners || !value)
      return partners;
    return partners.filter( partner =>
      partner.name.toLowerCase().indexOf(value.toLowerCase()) !== -1 );
  }

}
