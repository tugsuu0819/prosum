import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../services/product/product.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.page.html',
  styleUrls: ['./product.page.scss'],
})
export class ProductPage implements OnInit {

  constructor(
    private productService: ProductService
  ) {
    // this.productService.getProducts().then(data => {
    //   console.log('products', data);
    // })
  }

  ngOnInit() {
  }

}
