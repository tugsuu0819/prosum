import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OrderResolverService } from '../services/order/order-resolver.service';
import { ProductResolverService } from '../services/product/product-resolver.service';
import { PartnerResolverService } from '../services/partner/partner-resolver.service';

const routes: Routes = [
  {
    path: 'partner',
    loadChildren: './partner/partner.module#PartnerPageModule'
  },
  {
    path: 'order',
    loadChildren: './order/index.module#OrderPageModule' ,
    resolve: {
      orders: OrderResolverService,
      products: ProductResolverService,
      partners: PartnerResolverService
    }
  },
  {
    path: 'product',
    loadChildren: './product/product.module#ProductPageModule'
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MemberRoutingModule { }
