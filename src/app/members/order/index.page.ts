import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';

import * as _ from 'lodash';
import * as moment from 'moment';

import { OrderService } from '../../services/order/order.service';
import { PartnerService } from '../../services/partner/partner.service';
import { NavService } from '../../services/nav/nav.service';

import { Order } from '../../models/order';

@Component({
  selector: 'app-order',
  templateUrl: './index.page.html',
  styleUrls: ['./index.page.scss'],
})
export class OrderPage implements OnInit {

  orders: Array<Order>;
  constructor(
    private orderService: OrderService,
    private navCtrl: NavController,
    private partnerService: PartnerService,
    private navService: NavService,
    private route: ActivatedRoute,
    private router: Router,
  ) {
    // this.orderService.fetchPurchaseDataFromServer().then(data => {
    //   console.log(data);
    // });
  }

  ngOnInit() {
    // this.route.data
    //   .subscribe((data: {orders: Order[]}) => {
    //     console.log(data);
    //   })
    this.orders = this.orderService.getOrders();
  }

  createOrder() {
    this.navCtrl.navigateForward(['members', 'order', 'create']);
    // this.router.navigate(['members', 'partner']);
  }
  onViewOrder(order){
    this.navService.set(order);
    this.navCtrl.navigateForward('/members/order/detail/'+ order.id);
    // console.log(order);
  }
  onCreateOrder(){
    console.log("clicked");
  }
}
