import { Component, OnInit, ViewChild } from '@angular/core';
import { NavController } from '@ionic/angular';

import { PartnerService } from '../../../services/partner/partner.service';
import { ProductService } from '../../../services/product/product.service';
import { PriceListService } from '../../../services/priceList/price-list.service';
import { OrderService } from '../../../services/order/order.service';

import { Product } from '../../../models/product';
import { Partner } from '../../../models/partner';
import { PriceList } from '../../../models/price-list';

@Component({
  selector: 'app-create-order',
  templateUrl: './create.page.html',
  styleUrls: ['./create.page.scss'],
})
export class CreatePage implements OnInit {

  private partners : Partner[];
  private tPartner : Partner[];
  private products : Product[];
  private pricelist : PriceList[];
  @ViewChild("srcPartner") srcPartner;
  constructor(
    private navCtrl : NavController,
    private partnerService: PartnerService,
    private productService: ProductService,
    private priceListService: PriceListService,
    private orderService: OrderService
  ) {}

  ngOnInit() {
    this.prepPartners();
    this.prepProducts();
    this.prepPriceLists();
  }
  private prepPartners(){
    this.partners = this.partnerService.getPartners();
    this.tPartner = this.partnerService.getPartners();
  }
  private prepProducts(){

  }
  private prepPriceLists(){

  }
  searchPartner(evt){
    if(!this.srcPartner.value)
      this.tPartner = this.partners;
    this.tPartner =  this.partners.filter( partner =>
      partner.name.toLowerCase().indexOf(this.srcPartner.value.toLowerCase()) !== -1 );
  }
  onPartnerSelect(partner){
    this.orderService.create_o_set_partner(partner.id);
    this.navCtrl.navigateForward(["members", "order", "create-phase2"]);
  }
}
