import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import * as _ from 'lodash';

import { Order } from "../../../models/order";
import { OrderService } from "../../../services/order/order.service";
import { ProductService } from "../../../services/product/product.service";


@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {
  order : Order;

  constructor(
    private route : ActivatedRoute,
    private orderService : OrderService,
    private productService : ProductService
  ) { }

  ngOnInit() {
    this.prepOrder();
  }

  prepOrder() : void {
    const id = +this.route.snapshot.paramMap.get('id');
    let order = this.orderService.getOrder(id);
    // console.log(order);
    _.forEach(order.ln, (order_product) => {
      let product = this.productService.getProduct(order_product.prod_id);
      order_product.barcode = product.barcode;
      order_product.price = order_product.pu;
      order_product.total_price = order_product.pu * order_product.qty;
    });
    this.order = order;
  }

}
