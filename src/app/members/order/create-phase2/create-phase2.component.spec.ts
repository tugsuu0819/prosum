import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatePhase2Component } from './create-phase2.component';

describe('CreatePhase2Component', () => {
  let component: CreatePhase2Component;
  let fixture: ComponentFixture<CreatePhase2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatePhase2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatePhase2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
