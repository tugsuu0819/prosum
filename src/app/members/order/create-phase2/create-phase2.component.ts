import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

import * as _ from 'lodash';

import { PartnerService } from '../../../services/partner/partner.service';
import { ProductService } from '../../../services/product/product.service';
import { PriceListService } from '../../../services/priceList/price-list.service';
import { OrderService } from '../../../services/order/order.service';

import { Product } from '../../../models/product';
import { Partner } from '../../../models/partner';
import { PriceList } from '../../../models/price-list';
import { OrderCreateLine } from '../../../models/order-create';

@Component({
  selector: 'app-create-phase2',
  templateUrl: './create-phase2.component.html',
  styleUrls: ['./create-phase2.component.scss']
})
export class CreatePhase2Component implements OnInit {

  private partners : Partner[];
  private preInfos : any;
  private products : Product[];
  private pricelist : PriceList[];
  private showRanger : number = -1;
  private c_product : OrderCreateLine = { product_qty: 0, product_id: -1 };
  private c_products : OrderCreateLine[] = [];
  private showCreateCta : boolean = false;
  constructor(
    private navCtrl : NavController,
    private partnerService: PartnerService,
    private productService: ProductService,
    private priceListService: PriceListService,
    private orderService: OrderService
  ) { }

  ngOnInit() {
    this.prepProducts();
  }
  private prepProducts(){
    this.prepPreInfo();
    this.products = this.productService.getProducts();
    _.forEach(this.products, (product) => {
      product.price = this.productService.getPrice(this.preInfos.partnerId, product.id);
    });
    console.log(this.products);
  }
  private prepPreInfo(){
    let selPartnerId = this.orderService.get_create_o_partner();
    let partnerInfo = this.partnerService.getPartner(selPartnerId);
    if(partnerInfo){
      this.preInfos = {
        partnerName: partnerInfo.name,
        partnerId: selPartnerId,
        productCount: 0
      };
    }
  }

  onProductClick(product){
    if(this.showRanger == product.id)
      this.showRanger = -1;
    else
      this.showRanger = product.id;
  }

  onRangeChange(evt, product){
    this.c_product.product_qty = evt.detail.value;
    this.c_product.product_id = product.id;
    this.c_product.price = product.price;

  }
  onProductCtaClick(isOk : boolean){
    if(isOk && this.c_product.product_qty > 0) {
      let index = _.findIndex(this.c_products, {product_id: this.c_product.product_id});
      if(index == -1)
        this.c_products.push(this.c_product);
      else
        this.c_products[index] = this.c_product;
      this.preInfos.productCount = this.c_products.length;
      if(this.c_products.length > 0)
        this.showCreateCta = true;
    }
    console.log(this.c_products);
    this.resetRanger();
  }
  private resetRanger(){
    this.showRanger = -1;
    this.c_product = {
      product_qty : 0,
      product_id: -1
    }
  }
  confirmOrder(){
    this.orderService.create_o_set_orderline(this.c_products);
    this.navCtrl.navigateForward(["members", "order", "create-phase3"]);
  }
}
