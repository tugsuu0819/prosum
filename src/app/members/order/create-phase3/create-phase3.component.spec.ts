import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatePhase3Component } from './create-phase3.component';

describe('CreatePhase3Component', () => {
  let component: CreatePhase3Component;
  let fixture: ComponentFixture<CreatePhase3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatePhase3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatePhase3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
