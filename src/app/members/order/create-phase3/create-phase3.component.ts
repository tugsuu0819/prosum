import { Component, OnInit } from '@angular/core';
import { NavController, AlertController } from '@ionic/angular';

import * as _ from 'lodash';

import { PartnerService } from '../../../services/partner/partner.service';
import { ProductService } from '../../../services/product/product.service';
import { PriceListService } from '../../../services/priceList/price-list.service';
import { OrderService } from '../../../services/order/order.service';

import { Product } from '../../../models/product';
import { Partner } from '../../../models/partner';
import { PriceList } from '../../../models/price-list';
import { OrderCreateLine } from '../../../models/order-create';


@Component({
  selector: 'app-create-phase3',
  templateUrl: './create-phase3.component.html',
  styleUrls: ['./create-phase3.component.scss']
})
export class CreatePhase3Component implements OnInit {
  private partners : Partner[];
  private preInfos : any;
  private products : Product[];
  private product_lines : object[] = [];
  constructor(
    private navCtrl : NavController,
    private partnerService: PartnerService,
    private productService: ProductService,
    private priceListService: PriceListService,
    private orderService: OrderService,
    private alertController: AlertController
  ) { }

  ngOnInit() {
    this.prepData();
  }
  async presentAlertMultipleButtons() {
    const alert = await this.alertController.create({
      header: 'Төлбөрийн сонголт',
      buttons: [
        {
          text: 'Бэлэн',
          handler: () => {
              console.log("belen");
              this.presentAlert();
          }
        },
        {
          text: 'Зээл',
          handler: () => {
            this.presentAlert();
            console.log("Зээл");
          }
        }
      ]
    });
    await alert.present();
  }
  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Амжилттай',
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            this.navCtrl.navigateRoot(["members", "order"]);
          }
        }
      ]
    });

    await alert.present();
  }
  prepData(){
    let dt = this.orderService.get_create_o_data();
    this.preInfos = this.partnerService.getPartner(dt.partner_id);
    this.preInfos.total_price = 0;
    let partner_ln : object[] = [];
    _.forEach(dt.order_line, (ln) => {
      let prodInfo = this.productService.getProduct(ln.product_id);
      prodInfo.product_qty = ln.product_qty;
      prodInfo.product_price = ln.price;
      prodInfo.product_tPrice = (ln.product_qty * ln.price);
      this.preInfos.total_price += prodInfo.product_tPrice;
      partner_ln.push(prodInfo);
    });
    this.product_lines = partner_ln;
    // console.log(this.preInfos);
    // console.log(this.product_lines);
  }
  confirmOrder(){
    this.presentAlertMultipleButtons();
  }
}
