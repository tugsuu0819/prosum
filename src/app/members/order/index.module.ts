import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { OrderPage } from './index.page';
import { CreatePage } from './create/create.page';
import { DetailComponent } from './detail/detail.component';
import { CreatePhase2Component } from './create-phase2/create-phase2.component';
import { CreatePhase3Component } from './create-phase3/create-phase3.component';

const routes: Routes = [
  {
    path: '',
    component: OrderPage
  },
  {
    path: 'create',
    component: CreatePage
  },
  {
    path: 'create-phase2',
    component: CreatePhase2Component
  },
  {
    path: 'create-phase3',
    component: CreatePhase3Component
  },
  {
    path: 'detail/:id',
    component: DetailComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [OrderPage, CreatePage, DetailComponent, CreatePhase2Component, CreatePhase3Component]
})
export class OrderPageModule {}
