import { Component, OnInit } from '@angular/core';
import { PartnerService } from '../../services/partner/partner.service';
import { NavService } from '../../services/nav/nav.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-partner',
  templateUrl: './partner.page.html',
  styleUrls: ['./partner.page.scss'],
})
export class PartnerPage implements OnInit {
  items: Array<any>;
  constructor(
    private partnerService: PartnerService,
    private navService: NavService,
    private navCtrl: NavController
  ) {}

  ngOnInit() {
  }

  async itemClick(order){
    // console.log(order);
    this.navService.set(order);
    await this.navCtrl.navigateForward(['members', 'order', 'index']);
  }
}
