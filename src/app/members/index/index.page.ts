import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

import { OrderService } from '../../services/order/order.service';
import { NavService } from '../../services/nav/nav.service';

@Component({
  selector: 'app-member-index',
  templateUrl: './index.page.html',
  styleUrls: ['./index.page.scss'],
})
export class IndexPage implements OnInit {
  orders : [];
  constructor(
    private router : NavController,
    private orderService : OrderService,
    private navService : NavService
  )
  { }

  ngOnInit() {
    // this.orderService.getOrders().then((resp) => {
    //   console.log(resp);
    //   this.orders = resp;
    // });
  }
  onViewOrder(order){
    this.navService.set(order);
    this.router.navigateForward('/members/order/detail/'+ order.id);
    // console.log(order);
  }
  onCreateOrder(){
    console.log("clicked");
  }

}
