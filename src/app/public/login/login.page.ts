import { Component, OnInit, ViewChild } from '@angular/core';
import { NavController, AlertController } from '@ionic/angular';
import { MainService } from '../../services/main/main.service';
// import { OrderPage } from '../../members/order/index.page';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  @ViewChild('username') uname;
  @ViewChild('password') password;

  constructor(
    public navCtrl: NavController,
    // private router: Router,
    private mainService: MainService,
    private alertCtrl: AlertController
  ) {

  }

  signIn(){

    let sendData = {
      username: this.uname.value,
      password: this.password.value
    };
    this.navCtrl.navigateRoot(['members','order']);
    // this.mainService.get("partners", {}).then(data => {
    //   console.log(data);
    // }, err => {
    //   console.log(err);
    // });
    // this.mainService.login(sendData).then(data => {
    //   console.log('here');
    //   console.log(data);
    //   this.navCtrl.navigateRoot(['members','index']);
    //
    //   // this.navCtrl.setRoot(PurchasePage);
    //   // this.socketProvider.sendUserNameToService(this.uname.value);
    //
    // }, async err => {
    //   let alert = await this.alertCtrl.create({
    //     header: 'Нэр эсвэл нууц үг буруу байна',
    //     buttons: [
    //       {
    //         text: 'Ok'
    //       }
    //     ]
    //   });
    //   await alert.present();
    //   console.log(err);
    // });
    // // console.log('here');
    // // this.navCtrl.navigateRoot(['members','index']);
    // // this.router.navigate(['members', 'order']);
  }
  ngOnInit() {
  }

}
